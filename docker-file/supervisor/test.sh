#!/bin/bash
while true
do 
	# Echo current date to stdout
	echo `date` >> date.out
	# Echo 'error!' to stderr
	echo 'error!' >&2
	sleep 1
done
