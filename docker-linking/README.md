$ docker run -d --name db training/postgres

$ docker run -d -P --name web --link db:mydb training/webapp python app.py

$ docker inspect -f "{{ .HostConfig.Links }}" web

$ docker exec -it web bash
# ping mydb
root@a65313f62d87:/opt/webapp# ping mydb
PING mydb (172.17.0.2) 56(84) bytes of data.
64 bytes from mydb (172.17.0.2): icmp_seq=1 ttl=64 time=0.071 ms
64 bytes from mydb (172.17.0.2): icmp_seq=2 ttl=64 time=0.051 ms


