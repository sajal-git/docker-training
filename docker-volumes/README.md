
$ docker run -d -P --name web -v /opt/webapp:/webapp training/webapp python app.py
3daa28b51db0a11d9f896cfce926906c06fbde1d75b67705c72fc2df242ab476

$ docker exec -it web bash
# ls -l /webapp/
total 0

$ docker run -d -P --name web -v /opt/webapp:/opt/webapp -v /opt/appdata:/webapp training/webapp python app.py
1d6dc372dbf6c40a8042bec404f5d572a3bf591d70f30276fabd09aaa56bda29

$ docker inspect --format '{{json .Mounts}}' web
[{"Source":"/opt/appdata","Destination":"/webapp","Mode":"","RW":true,"Propagation":"rprivate"},{
  "Source":"/opt/webapp","Destination":"/opt/webapp","Mode":"","RW":true,"Propagation":"rprivate"}]


