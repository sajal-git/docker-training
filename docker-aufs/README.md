
$ vim Dockerfile

FROM ubuntu:16.04

RUN echo "Hello world" > /tmp/newfile


$ docker build -t myubuntu .
Sending build context to Docker daemon 2.048 kB
Step 1 : FROM ubuntu:16.04
 ---> 104bec311bcd
Step 2 : RUN echo "Hello world" > /tmp/newfile
 ---> Running in 2559d295f18a
 ---> 15ffbee08645
Removing intermediate container 2559d295f18a
Successfully built 15ffbee08645

$ docker images
REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
myubuntu                latest              35fc4f751dce        5 minutes ago       129 MB
ubuntu                  16.04               104bec311bcd        2 weeks ago         129 MB


$ docker history ubuntu:16.04
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
104bec311bcd        2 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/bash"]            0 B
<missing>           2 weeks ago         /bin/sh -c mkdir -p /run/systemd && echo 'doc   7 B
<missing>           2 weeks ago         /bin/sh -c sed -i 's/^#\s*\(deb.*universe\)$/   1.895 kB
<missing>           2 weeks ago         /bin/sh -c rm -rf /var/lib/apt/lists/*          0 B
<missing>           2 weeks ago         /bin/sh -c set -xe   && echo '#!/bin/sh' > /u   745 B
<missing>           2 weeks ago         /bin/sh -c #(nop) ADD file:7529d28035b43a2281   129 MB

$ docker history myubuntu
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
35fc4f751dce        13 seconds ago      /bin/sh -c echo "Hello world" > /tmp/newfile    12 B
104bec311bcd        2 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/bash"]            0 B
<missing>           2 weeks ago         /bin/sh -c mkdir -p /run/systemd && echo 'doc   7 B
<missing>           2 weeks ago         /bin/sh -c sed -i 's/^#\s*\(deb.*universe\)$/   1.895 kB
<missing>           2 weeks ago         /bin/sh -c rm -rf /var/lib/apt/lists/*          0 B
<missing>           2 weeks ago         /bin/sh -c set -xe   && echo '#!/bin/sh' > /u   745 B
<missing>           2 weeks ago         /bin/sh -c #(nop) ADD file:7529d28035b43a2281   129 MB


